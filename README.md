**_Fortumo Prooviülesanne_**

See on lühike ülevaade selelst mis ma tegin.

**Arenduskeskkond**

Kasutasin IntelliJ IDEA, Maven, Lombok ja Spring teekidega.

**Arhitektuur**

`com.example` all on kaustad `controllers` ja `models`.
`controllers` sees on REST controller, mis annab andmebaasist andmeid JSON-ga.
`models` all on andmebaasikood.

**Jooksutamine**

Et saada Subscription Infot, tuleks get päringu saata `/subs/sub` URLile.
See nõuab parameetrit nimega `id`, mis peaks vastama Subscription ID-le.

Niisama debugimiseks on jäänud URL `/subs` mis annab terve SubscriptionInfoEntryRepository
sisu JSON kujul.

Kogu lahendus on heroku serveri peal üleval. Link sellele: https://morning-tor-32088.herokuapp.com