insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (1, 'Mari Maasikas', 'Subscription 1', 'Us', '2017-08-01');
insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (2, 'Mart Maasikas', 'Subscription 1', 'Us', '2017-08-01');
insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (3, 'Merike Mets', 'Subscription 1', 'Us', '2017-08-01');
insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (4, 'Siim Sisalik', 'Subscription 1', 'Us', '2017-08-01');
insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (5, 'Siiri Sisalik', 'Subscription 2', 'Us', '2017-09-01');
insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (6, 'Mari Maasikas', 'Subscription 2', 'Us', '2017-09-01');
insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (7, 'Mart Maasikas', 'Subscription 2', 'Us', '2017-09-01');
insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (8, 'Merike Mets', 'Subscription 2', 'Us', '2017-09-01');
insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (9, 'Siim Sisalik', 'Subscription 2', 'Us', '2017-09-01');
insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (10, 'Siiri Sisalik', 'Subscription 2', 'Us', '2017-09-01');
insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (11, 'Mari Maasikas', 'Subscription 3', 'Us', '2017-07-01');
insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (12, 'Mart Maasikas', 'Subscription 3', 'Us', '2017-07-01');
insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (13, 'Merike Mets', 'Subscription 3', 'Us', '2017-07-01');
insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (14, 'Siim Sisalik', 'Subscription 3', 'Us', '2017-07-01');
insert into subscription_info_entry (id, client_name, service_name, service_provider, next_billing_date) values (15, 'Siiri Sisalik', 'Subscription 3', 'Us', '2017-07-01');

insert into payment_info_entry (id, subscription_info_id, date, start_date, end_date, total_paid) values (1, 1, '2016-01-01', '2016-01-01', '2017-01-01', 500)
insert into payment_info_entry (id, subscription_info_id, date, start_date, end_date, total_paid) values (2, 2, '2016-01-01', '2016-01-01', '2017-01-01', 500)
insert into payment_info_entry (id, subscription_info_id, date, start_date, end_date, total_paid) values (3, 3, '2016-01-01', '2016-01-01', '2017-01-01', 500)
insert into payment_info_entry (id, subscription_info_id, date, start_date, end_date, total_paid) values (4, 4, '2016-01-01', '2016-01-01', '2017-01-01', 500)
insert into payment_info_entry (id, subscription_info_id, date, start_date, end_date, total_paid) values (5, 5, '2016-01-01', '2016-01-01', '2017-01-01', 500)
insert into payment_info_entry (id, subscription_info_id, date, start_date, end_date, total_paid) values (6, 6, '2016-01-01', '2016-01-01', '2017-01-01', 500)



