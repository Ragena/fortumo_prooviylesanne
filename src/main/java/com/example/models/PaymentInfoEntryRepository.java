package com.example.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentInfoEntryRepository extends JpaRepository<PaymentInfoEntry, Long>{
    @Query("select p from PaymentInfoEntry p where p.id = :id")
    List<PaymentInfoEntry> findByID(@Param("id") Long id);
}
