package com.example.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
public class SubscriptionInfoEntry {
    @Id @GeneratedValue
    Long id;

    String clientName;
    String serviceName;
    String serviceProvider;
    LocalDate nextBillingDate;

    //@OneToMany
    //List<PaymentInfoEntry> paymentInfoEntries;
}
