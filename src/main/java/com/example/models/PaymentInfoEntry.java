package com.example.models;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
public class PaymentInfoEntry {
    @Id @GeneratedValue
    Long id;

    LocalDate date;

    @Embedded
    BusinessPeriod servicePeriod;
    BigDecimal totalPaid;

    @ManyToOne
    SubscriptionInfoEntry subscriptionInfo;
}
