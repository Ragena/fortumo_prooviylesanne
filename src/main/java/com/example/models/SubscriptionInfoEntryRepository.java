package com.example.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubscriptionInfoEntryRepository extends JpaRepository<SubscriptionInfoEntry, Long>{
    @Query("select sub from SubscriptionInfoEntry sub where sub.id = :id")
    List<SubscriptionInfoEntry> findByID(@Param("id") Long id);
}
