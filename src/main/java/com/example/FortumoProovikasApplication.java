package com.example;

import com.example.models.PaymentInfoEntry;
import com.example.models.PaymentInfoEntryRepository;
import com.example.models.SubscriptionInfoEntry;
import com.example.models.SubscriptionInfoEntryRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.time.LocalDate;

@SpringBootApplication
public class FortumoProovikasApplication {

	public static void main(String[] args) {
		SpringApplication.run(FortumoProovikasApplication.class, args);
	}
}
