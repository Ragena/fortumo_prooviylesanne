package com.example.controllers;

import com.example.models.SubscriptionInfoEntry;
import com.example.models.SubscriptionInfoEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SubscriptionInfoEntryController {
    @Autowired
    SubscriptionInfoEntryRepository repo;

    @GetMapping("/subs")
    public List<SubscriptionInfoEntry> findAll()
    {
        return repo.findAll();
    }

    @RequestMapping(value = "/subs/sub", method = RequestMethod.GET)
    public List<SubscriptionInfoEntry> findById(
            @RequestParam(value = "id") Long id
    )
    {
        List<SubscriptionInfoEntry> sub = null;
        if (id != null) {
            System.out.println("ID: " + id);
            sub = repo.findByID(id);
        }

        return sub;
    }
}
