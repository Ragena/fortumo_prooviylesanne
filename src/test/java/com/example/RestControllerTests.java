package com.example;

import com.example.models.PaymentInfoEntry;
import com.example.models.PaymentInfoEntryRepository;
import com.example.models.SubscriptionInfoEntry;
import com.example.models.SubscriptionInfoEntryRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = FortumoProovikasApplication.class)
public class RestControllerTests {
    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private SubscriptionInfoEntryRepository subRepo;
    @Autowired
    private PaymentInfoEntryRepository paymentRepo;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void testGetAllSubs() throws Exception {
        mockMvc.perform(get("/subs"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    public void testGetById() throws Exception {
        SubscriptionInfoEntry sub = subRepo.findOne(1L);
        mockMvc.perform(get("/subs/sub")
                .param("id", "1")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].clientName", is(sub.getClientName())))
                .andExpect(jsonPath("$.[0].serviceName", is(sub.getServiceName())))
                .andExpect(jsonPath("$.[0].serviceProvider", is(sub.getServiceProvider())));

    }
}
