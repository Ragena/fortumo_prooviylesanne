package com.example;

import com.example.models.PaymentInfoEntry;
import com.example.models.PaymentInfoEntryRepository;
import com.example.models.SubscriptionInfoEntry;
import com.example.models.SubscriptionInfoEntryRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = FortumoProovikasApplication.class)
public class RepositoryTests {
    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private SubscriptionInfoEntryRepository subRepo;
    @Autowired
    private PaymentInfoEntryRepository paymentRepo;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void findByIdTest() {
        SubscriptionInfoEntry subscriptionInfoEntry = subRepo.findOne(1l);

        assertThat(subRepo.findByID(subscriptionInfoEntry.getId())).contains(subscriptionInfoEntry);

        PaymentInfoEntry paymentInfoEntry = paymentRepo.findOne(1l);

        assertThat(paymentRepo.findByID(paymentInfoEntry.getId())).contains(paymentInfoEntry);
    }
}
